import os
import psycopg2
import pandas as pd

POSTGRES_URI = os.environ.get('POSTGRES_URI')


def get_exams_subject():
    try:
        conn = psycopg2.connect(POSTGRES_URI)
        sql = "SELECT subject, exam_date FROM gradebook_records ORDER BY subject;"
        data = pd.read_sql_query(sql, conn)
        conn.close()
        return data
    except Exception as e:
        return f"PostgreSQL connect: Failed: Exception: {e}"

exams = get_exams_subject()
print(f"Table of the subject exams")
print(exams)
