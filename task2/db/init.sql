CREATE TABLE IF NOT EXISTS gradebook
(
    id INT PRIMARY KEY NOT NULL,
    institute character varying(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS students
(
    id SERIAL PRIMARY KEY NOT NULL,
    first_name character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    date_birthday DATE NOT NULL,
    gradebook INT REFERENCES gradebook(id) NOT NULL
);

CREATE TABLE IF NOT EXISTS gradebook_records
(
    id SERIAL PRIMARY KEY NOT NULL,
    gradebook_id INT REFERENCES gradebook(id),
    subject character varying(100) NOT NULL,
    exam_date DATE NOT NULL,
    lecturer character varying(200) NOT NULL
);

INSERT INTO gradebook (id, institute)
VALUES
    (30242, 'ИКБ'),
    (30343, 'ИКБ'),
    (30543, 'ИИ');

INSERT INTO students (first_name, name, date_birthday, gradebook)
VALUES
    ('Иванов', 'Иван', '2002-02-16', 30242),
    ('Федоров', 'Павел', '2002-05-3', 30343),
    ('Петрова', 'Анастасия', '2002-07-12', 30543);

INSERT INTO gradebook_records (gradebook_id, subject, exam_date, lecturer)
VALUES
    (30242, 'Сетевые технологии', '2023-01-10', 'Борисов С.П.'),
    (30242, 'Теория вероятности', '2023-01-15', 'Волкова Т.Г.'),
    (30242, 'Инструментальный анализ защищенности', '2023-01-24', 'Воробьев А.А.'),
    (30343, 'Сетевые технологии', '2023-01-11', 'Борисов С.П.'),
    (30343, 'Теория вероятности', '2023-01-17', 'Волкова Т.Г.'),
    (30343, 'Инструментальный анализ защищенности', '2023-01-26', 'Воробьев А.А.'),
    (30543, 'Машинное обучение', '2023-01-19', 'Дмитриев М.А.'),
    (30543, 'Теория вероятности', '2023-01-12', 'Волкова Т.Г.'),
    (30543, 'Обеспечение обработки больших данных', '2023-01-29', 'Рогов К.А.');
