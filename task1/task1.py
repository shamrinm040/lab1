import os

str = os.environ.get("INPUT_ARG")

if str.isdigit():
    num = int(str)
    print(f'Восьмеричное: {num:o}')
    print(f'Шестнадцатеричное: {num:x}')
else:
    print(f'"{str}" не является целочисленным числом')
